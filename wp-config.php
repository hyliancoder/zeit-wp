<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $_ENV['DB_NAME']);

/** MySQL database username */
define('DB_USER', $_ENV['DB_USER']);

/** MySQL database password */
define('DB_PASSWORD', $_ENV['DB_PASSWORD']);

/** MySQL hostname */
define('DB_HOST', $_ENV['DB_HOST']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'AgvBou$TL]El=g#8xBYbx^w7E4){M6y  (t6;w4eE0Z?zo^Y:!@b9jLZfdS~ttJH');
define('SECURE_AUTH_KEY',  'W,f3:qo>]lg@po m<JJi3A(+{J@7:UdJ[e3h=C][0oR;@GWz#Sye: -Km~oBN!X(');
define('LOGGED_IN_KEY',    '=P=u/36-L<S.BiV(y~=Wj(.V<1=?Q!P+1>E&>|HM>E(Khte|l`])fi@E|dUTl@+P');
define('NONCE_KEY',        '#9Dk+6hwl?Q@.EE-=+f5P&w)-Y<g}:?UA+<%gFxD&?]lGM/MwJ._m~V7{p%cZK*z');
define('AUTH_SALT',        'm-YXUr[f>X|}h>CSf|B~gGs5xdwD&Jhj|ex1?nsxo2m>%K7pRJQ=cMO|.z*~VVa5');
define('SECURE_AUTH_SALT', '0GVh|B|&@-)S7rwc C0OPaU!2?,}kh3V|e {c(Ki)m3H ]XJ_]Kewdyd47)/V,$*');
define('LOGGED_IN_SALT',   '^S?%C5hv_N|Sz;=: `oTpZRObwgB &9M6KG+^LsOEJm[!ruwlh&D+`m)t`}=(UV|');
define('NONCE_SALT',       '}pq%tRa5awIzZ6~XM35Rv`;(m)p6CS#79Uc6|3dFWu>5AJ<?9y*o/)SWf<GEivj_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define( 'WP_SITEURL', 'https://' . $_SERVER['HTTP_HOST'] );
define( 'WP_HOME', 'https://' . $_SERVER['HTTP_HOST'] );
define( 'WP_CONTENT_URL', 'https://' . $_SERVER['HTTP_HOST'] . '/wp-content' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
        define('ABSPATH', dirname(__FILE__) . '/');


/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');